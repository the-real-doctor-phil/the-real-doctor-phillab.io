#+title: About this blog
#+author: Simon Johansson
#+date:<2019-02-13 Wed> 

[[http://orgmode.org][org-mode]] blog using [[http://pages.gitlab.io/][GitLab Pages]]. See [[https://pages.gitlab.io/org-mode][org-mode on Gitlab pages]] to recreate this.

Visit the live blog at https://the-real-doctor-phil.gitlab.io
